/**
 * Simple Web Graphics Library
 *
 * WebGL 2D renderer and scenegraph
 * Main interface class
 */

/// <reference path="Renderer.ts" />
/// <reference path="Texture.ts" />
/// <reference path="Font.ts" />

/// <reference path="math/Matrix2D.ts" />
/// <reference path="math/Vec2.ts" />

/// <reference path="scene/Node.ts" />
/// <reference path="scene/Sprite.ts" />
/// <reference path="scene/Text.ts" />

module SWGL {

    export class SWGL {

        private _canvas: HTMLCanvasElement;
        private _renderer: Renderer;

		/**
		 * Create an SWGL context for an HTML Canvas
		 * SWGL takes over control of the canvas element.
		 */
        constructor(canvas: HTMLCanvasElement) {
            this._canvas = canvas;
            this._renderer = new Renderer(canvas);
        }

		/**
		 * Get access to the renderer object bound to
		 * this SWGL instance
		 */
        public getRenderer(): Renderer {
            return this._renderer;
        }

    }

}
