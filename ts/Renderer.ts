/**
 * SWGL Renderer
 *
 * Buffers draw instructions before passing them
 * on to WebGL, to avoid hiccups and allow for
 * tighter processing time.
 */

/// <reference path="math/Matrix2D.ts" />

module SWGL {

    export class Renderer {

        private static __vertexShaderSource = "" +
        "// vertex shader";

        private static __fragmentShaderSource = "" +
        "// fragment shader";

        private _canvas: HTMLCanvasElement;
        private _gl: WebGLRenderingContext;

        private _program: WebGLProgram;

        constructor(canvas: HTMLCanvasElement) {

            this._canvas = canvas;
            var gl = this._gl = canvas.getContext("webgl");

            var vertexShader = gl.createShader(gl.VERTEX_SHADER);
            gl.shaderSource(vertexShader, __vertexShaderSource);
            gl.compileShader(vertexShader);

            if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
                console.error(gl.getShaderInfoLog(vertexShader));
                throw "Vertex shader compile failed";
            }

            var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
            gl.shaderSource(fragmentShader, __fragmentShaderSource);
            gl.compileShader(fragmentShader);

            if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
                console.error(gl.getShaderInfoLog(fragmentShader));
                throw "Fragment shader compile failed";
            }

            var program = this._program = gl.createProgram();
            gl.attachShader(program, vertexShader);
            gl.attachShader(program, fragmentShader);
            gl.linkProgram(program);
            if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
                console.error(gl.getProgramInfoLog(program));
                throw "Program link failed";
            }

        }

        public setMatrix(mtx: Matrix2D): void {

        }

        public drawQuads(vertices: Float32Array, uv: Float32Array, colors: Float32Array, num: Number): void {

        }

    }

}

