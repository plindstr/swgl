Simple Web Graphics Library
===========================

SWGL takes care of the boilerplate of setting up and managing
a WebGL environment, and provides an object-oriented API for
resource management, as well as a simple, extensible scene graph.

The goal is to allow WebGL graphics to be used in an application,
while retaining readability and reducing complexity.
